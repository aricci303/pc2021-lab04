package acme;

import cartago.*;

public class MyEnv extends Artifact {
	
	void init(int initialValue){ 
		this.defineObsProperty("count", initialValue);
	}
	
	@OPERATION void inc(){
		ObsProperty p = getObsProperty("count"); 
		p.updateValue(p.intValue() + 1); 
	} 

	@OPERATION void dec(){
		ObsProperty p = getObsProperty("count"); 
		p.updateValue(p.intValue() - 1); 
	} 
}
