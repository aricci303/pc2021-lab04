// Agent speaker_2 in project test2

/* Initial beliefs and rules */

/* Initial goals */

// !start.

+v(X) [source(Who)]
	<- .println("new belief acquired: ", v(X), " by ", Who);
	   .send(Who, achieve, g(X)).
	   
	

/* Plans */

// +!start : true <- .print("hello world.").

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
//{ include("$moiseJar/asl/org-obedient.asl") }
