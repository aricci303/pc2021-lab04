// Agent speaker_1 in project test2

/* Initial beliefs and rules */

/* Initial goals */

!start.

/* Plans */

+!start : true <- 
	.send(speaker_2, tell, v(10));
    .print("msg sent.").
    
+!g(X)[source(Who)]
	<- .println("new goal by ", Who, "- going to achieve it ", g(X)).
	   

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
//{ include("$moiseJar/asl/org-obedient.asl") }
