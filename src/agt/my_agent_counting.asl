// Agent my_agent_counting in project test2

/* Initial beliefs and rules */

/* Initial goals */

// !count(100).

// !bring_count_to(100).


/* Plans */

+!count(X) : not count(_)
	<- println("my_agent_counting started");
	   /* discover and observe my_env */
	   !prepare;
	   println("ready to work");
	   !count(X). 
	   	   
+!count(X) : count(X)
	<- println("achieved").
	
+!count(X) : count(Y) & Y < X
	<- println("too small (", Y, ") - incrementing...");
	   inc;
	   !count(X).

+!count(X) : count(Y) & Y > X
	<- println("too big (", Y, ") - decrementing...");
	   dec;
	   !count(X).
	   
	
	   

+!prepare
	<- lookupArtifact("my_env", Id);
	   focus(Id);
	   println("env discovered").

-!prepare
    <- println("env still not ready...");
       .wait(100);
       !prepare.
       
	   

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
//{ include("$moiseJar/asl/org-obedient.asl") }
